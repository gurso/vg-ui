import { test, expect } from "vitest"
import { mount } from "@vue/test-utils"
import SelectTest from "./SelectTest.vue"
import { VgOption } from "../src/lib"

test("displays message", async () => {
	const wrapper = mount(SelectTest)
	const options = wrapper.findAllComponents(VgOption)
	expect(options.length).toBe(3)
	const main = wrapper.find("span")
	await main.trigger("click")
	await options[1].trigger("click")
})
