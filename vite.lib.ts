/* eslint-env node */
import path from "path"
import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"

export default defineConfig({
	plugins: [
		vue({
			script: {
				defineModel: true,
			},
		}),
	],
	build: {
		minify: true,
		lib: {
			entry: path.resolve(__dirname, "src/lib.ts"),
			fileName: "index",
			name: "vg-ui",
			formats: ["es"],
		},
		rollupOptions: {
			// make sure to externalize deps that shouldn't be bundled
			// into your library
			external: ["vue"],
			output: {
				// Provide global variables to use in the UMD build
				// for externalized deps
				globals: {
					vue: "Vue",
				},
			},
		},
	},
})
