# Install

## Global plugin

In your main.js :

```js
import VgUi from "@gurso/vg-ui"
import { createApp } from "vue"
import App from "./App.vue"

createApp(App).use(VgUi).mount("#app")
```

or import each component when you need it:

```js
import { VgInput } from "@gurso/vg-ui"
```
