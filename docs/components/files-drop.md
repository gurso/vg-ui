<script setup lang="ts">
import { ref } from "vue"
import VgFilesDrop from "../../src/components/VgFilesDrop.vue"
import VgButton from "../../src/components/VgButton.vue"

const files = ref<File[]>([])

</script>

# files-drop

```vue-html
<VgFilesDrop v-model="files">
    <div v-if="files.length">
        <p v-for="file of files">
            {{ file.name }}
        </p>
    </div>
    <p v-else>
        Drop or click to select files
    </p>
</VgFilesDrop>
```

<VgFilesDrop v-model="files">
    <div v-if="files.length">
        <p v-for="file of files">
            {{ file.name }}
        </p>
    </div>
    <p v-else>
        Drop or click to select files
    </p>
</VgFilesDrop>
