<script setup lang="ts">
import { ref } from "vue"
import VgButton from "../../src/components/VgButton.vue"
import VgToast from "../../src/components/VgToast.vue"
import Toast from "../../src/components/toast.k"
const toastInfo = ref<Toast>()
const toastError = ref<Toast>()
</script>

<style scoped>
button, .toast {
    padding: 4px 8px;
    background: slateblue;
    border-radius: 4px;
    color:white;
    margin-inline: 8px;
}

.toast {
    margin: 0;
}

.error {
    background: tomato;
}

</style>

<VgButton @click="toastInfo?.start"> start </VgButton>
<VgButton @click="toastInfo?.pause"> pause </VgButton>
<VgButton @click="toastInfo?.stop"> stop </VgButton>

<VgToast ref="toastInfo" :duration="3000">
<p class="toast">
Information Toast
</p>
</VgToast>

<VgButton class="error" @click="toastError?.start"> start </VgButton>
<VgButton class="error" @click="toastError?.pause"> pause </VgButton>
<VgButton class="error" @click="toastError?.stop"> stop </VgButton>

<VgToast ref="toastError" :duration="3000">
<p class="toast error">
Error Toast
</p>
</VgToast>

# VgToast

```ts
import { Toast, VgToast, VgButton } from "@gurso/vg-ui"
const toastInfo = ref<Toast>()
```

```vue-html
<VgButton @click="toastInfo?.start"> start </VgButton>
<VgButton @click="toastInfo?.pause"> pause </VgButton>
<VgButton @click="toastInfo?.stop"> stop </VgButton>
<VgToast ref="toastInfo">
    <p class="toast">
        Information Toast
    </p>
</VgToast>
```
