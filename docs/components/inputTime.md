<script setup lang="ts">
import { ref } from "vue"
import VgInputTime from "../../src/components/VgInputTime.vue"

const time = ref("09:30")
</script>

# VgInputTime

```js
import { ref } from "vue"
const time = ref("")
```

```vue-html
<VgInputTime :step="1800" />
```

<VgInputTime v-model="time" :step="1800" />

<p> {{ time }} </p>
