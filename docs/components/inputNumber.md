<script setup lang="ts">
import { ref } from "vue"
import VgInputNumber from "../../src/components/VgInputNumber.vue"

const n = ref(0)

</script>

# InputNumber

```vue-html
    <VgInputNumber v-model="n" />
```

<VgInputNumber v-model="n" />

<pre>{{ n }}</pre>
