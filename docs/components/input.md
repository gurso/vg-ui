<script setup lang="ts">
import { ref } from "vue"
import VgInput from "../../src/components/VgInput.vue"

const txt = ref("test")

</script>

# Input

```vue-html
    <VgInput v-model="txt" />
```

<VgInput v-model="txt" />

<pre>{{ txt }}</pre>
