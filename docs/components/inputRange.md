<script setup lang="ts">
import { ref } from "vue"
import VgInputRange from "../../src/components/VgInputRange.vue"

const n = ref(0)

</script>

# InputNumber

```vue-html
    <VgInputRange v-model="n" />
```

<VgInputRange v-model="n" />

<pre>{{ n }}</pre>
