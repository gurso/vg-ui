<script setup lang="ts">
import { ref } from "vue"
import VgInputDate from "../../src/components/VgInputDate.vue"

const date = ref(new Date())
</script>

# VgInputDate

```js
import { ref } from "vue"
const date = ref(new Date())
```

```vue-html
<VgInputDate v-model="date" />
<p> {{ date }} </p>
```

<VgInputDate v-model="date" />
<p> {{ date }} </p>
