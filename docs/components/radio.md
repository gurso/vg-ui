<script setup lang="ts">
import { ref } from "vue"
import VgRadio from "../../src/components/VgRadio.vue"
import VgRadioLabel from "../../src/components/VgRadioLabel.vue"

const str = ref("")
</script>

# radio

<label>
    <VgRadio v-model="str" value="one"/>
    one
</label>

<label>
    <VgRadio v-model="str" value="two"/>
    two
</label>

<pre>{{ str }}</pre>

## radio label

<VgRadioLabel v-model="str" name="str" value="one"> one </VgRadioLabel>
<VgRadioLabel v-model="str" name="str" value="two"> two </VgRadioLabel>

<pre>{{ str }}</pre>
