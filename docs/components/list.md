<script setup lang="ts">
import { ref } from "vue"
import VgList from "../../src/components/VgList.vue"
import VgOption from "../../src/components/VgOption.vue"

const options = ref(["one", "two", "three"])
const selected = ref("two")

</script>

# Input

```vue-html
<VgList v-model="selected">
    <VgOption v-for="option of options" :key="option" :value="option" />
</VgList>
```

<VgList v-model="selected">
    <VgOption v-for="option of options" :key="option" :value="option">
        {{ option }}
    </VgOption>
</VgList>

<pre>{{ selected }}</pre>
