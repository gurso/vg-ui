<script setup lang="ts">
import { ref } from "vue"
import VgInputCheckbox from "../../src/components/VgInputCheckbox.vue"
import VgCheckLabel from "../../src/components/VgCheckLabel.vue"

const b = ref(false)
const s = ref("no")
const any = ref()
const arr = ref([])
</script>

# Checkbox

## boolean

```vue-html
<VgInputCheckbox v-model="b" />
```

<VgInputCheckbox v-model="b" /><pre>{{ b }}</pre>

## string value

```vue-html
<VgInputCheckbox true-value="yes" false-value="no" v-model="s" />
```

<VgInputCheckbox type="checkbox" true-value="yes" false-value="no" v-model="s" /><pre>{{ s }}</pre>

## object value

```vue-html
<VgInputCheckbox :true-value="{ test: 'foo'}" false-value="str" v-model="any" />
```

<VgInputCheckbox :true-value="{ test: 'foo'}" false-value="str" v-model="any" />
<pre>{{ any }}</pre>

## bind with an array

<VgInputCheckbox v-model="arr" value="test" />
<VgInputCheckbox v-model="arr" value="str" />
<VgInputCheckbox v-model="arr" :value="42" />

<pre>arr : {{arr}}</pre>

# CheckLabel

<VgCheckLabel v-model="b" v-slot="{ value }"> Text inside {{ value }} </VgCheckLabel>
