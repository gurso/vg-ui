import { defineConfig } from "vitepress"

// https://vitepress.dev/reference/site-config
export default defineConfig({
	base: "/vg-ui/",
	title: "My vue lib components",
	description: "My vue lib components",
	vue: {
		script: {
			defineModel: true,
		},
	},
	themeConfig: {
		// https://vitepress.dev/reference/default-theme-config
		nav: [{ text: "Home", link: "/install.md" }],

		sidebar: [
			{
				collapsed: false,
				text: "Introduction",
				items: [{ text: "Installation", link: "/install.md" }],
			},
			{
				collapsed: false,
				text: "Components",
				items: [
					{ text: "Simple Input", link: "/components/input.md" },
					{ text: "Input Number", link: "/components/inputNumber.md" },
					{ text: "Input Checkbox", link: "/components/inputCheckbox.md" },
					{ text: "Radio", link: "/components/radio.md" },
					{ text: "Input time", link: "/components/inputTime.md" },
					{ text: "Input date", link: "/components/inputDate.md" },
					{ text: "Input range", link: "/components/inputRange.md" },
					{ text: "Files drop", link: "/components/files-drop.md" },
					{ text: "data-list", link: "/components/list.md" },
					{ text: "Toast", link: "/components/toast.md" },
				],
			},
		],
	},
})
