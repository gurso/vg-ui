import { Ref } from "vue"

type XDirection = "left" | "right"
type YDirection = "top" | "bottom"
export type Direction = XDirection | YDirection
export type Directions = [XDirection, YDirection] | [YDirection, XDirection]
export const defaultStack = Symbol()
export const stacks = new Map<symbol, Map<string, Map<symbol, number>>>()

export type Exposed = {
	start: () => Promise<void>
	resume: () => Promise<void>
	stop: () => void
	pause: () => number
	paused: Ref<boolean>
}
