import { ComputedRef, InjectionKey, reactive, Ref, WritableComputedRef } from "vue"

export type Option<T = unknown> = { id: symbol; value: T; disabled?: boolean }
type Fn = () => void
export type Exposed<T = unknown> = {
	next: Fn
	previous: Fn
	select: Fn
	options: Readonly<Option<T>[]>
	target: Ref<Option<T> | undefined>
}

export const kDisabled: InjectionKey<ComputedRef<boolean | undefined>> = Symbol("disabled")
export const kOptions: InjectionKey<Option[]> = Symbol("options")
export const kSelected: InjectionKey<Ref<Option[]>> = Symbol("selected")
export const kTarget: InjectionKey<Ref<Option | undefined>> = Symbol("target")
export const kModel: InjectionKey<WritableComputedRef<unknown>> = Symbol("model")
export const kProps: InjectionKey<{ multiple?: boolean; id: symbol }> = Symbol("multiple")

// export const kWeakOptions: InjectionKey<WeakMap<HTMLLIElement, Option>> = Symbol("WeakOptions")
// export const kUl: InjectionKey<Ref<HTMLUListElement>> = Symbol("ul")
export const list = reactive(new Map<symbol, string>())
