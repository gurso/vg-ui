import { Plugin, App } from "vue"

import { useField } from "@gurso/vue-use-form"

import VgInput from "./components/VgInput.vue"
import VgInputTime from "./components/VgInputTime.vue"
import VgInputNumber from "./components/VgInputNumber.vue"
import VgInputDate from "./components/VgInputDate.vue"
import VgInputRange from "./components/VgInputRange.vue"
import VgInputCheckbox from "./components/VgInputCheckbox.vue"
import VgInputFiles from "./components/VgInputFiles.vue"
import VgButton from "./components/VgButton.vue"
import VgTextarea from "./components/VgTextarea.vue"
import VgRadio from "./components/VgRadio.vue"
import VgRadioLabel from "./components/VgRadioLabel.vue"
import VgCheckLabel from "./components/VgCheckLabel.vue"
import VgSwitch from "./components/VgSwitch.vue"
import VgToast from "./components/VgToast.vue"
import { Exposed as Toast } from "./components/toast.k"
import VgSelect from "./components/VgSelect.vue"
import VgList from "./components/VgList.vue"
import VgOption from "./components/VgOption.vue"
import VgOptgroup from "./components/VgOptgroup.vue"
import VgFilesDrop from "./components/VgFilesDrop.vue"

type ExposedField = ReturnType<typeof useField>

export type { Toast, ExposedField }

export {
	VgInput,
	VgInputTime,
	VgInputDate,
	VgInputCheckbox,
	VgTextarea,
	VgInputNumber,
	VgInputRange,
	VgRadio,
	VgRadioLabel,
	VgCheckLabel,
	VgButton,
	VgInputFiles,
	VgSwitch,
	VgToast,
	VgSelect,
	VgOption,
	VgOptgroup,
	VgList,
	VgFilesDrop,
}

export const plugin: Plugin = {
	install(app: App): void {
		app.component("VgInput", VgInput)
		app.component("VgTextarea", VgTextarea)
		app.component("VgInputTime", VgInputTime)
		app.component("VgInputDate", VgInputDate)
		app.component("VgInputNumber", VgInputNumber)
		app.component("VgInputRange", VgInputRange)
		app.component("VgInputFiles", VgInputFiles)
		app.component("VgButton", VgButton)
		app.component("VgRadio", VgRadio)
		app.component("VgRadioLabel", VgRadioLabel)
		app.component("VgCheckLabel", VgCheckLabel)
		app.component("VgSwitch", VgSwitch)
		app.component("VgToast", VgToast)
		app.component("VgList", VgList)
		app.component("VgSelect", VgSelect)
		app.component("VgOption", VgOption)
	},
}
export default plugin
