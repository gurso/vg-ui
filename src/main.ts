import { createApp } from "vue"
import App from "./App.vue"
import vg from "./lib"

createApp(App).use(vg).mount("#app")
