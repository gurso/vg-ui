import { InjectionKey, inject } from "vue"

export function strictInject<T>(k: InjectionKey<T>) {
	const t = inject(k)
	if (t) return t
	throw new Error("Bad Injection. Probably missing " + k.toString())
}
