type Numeric = { valueOf: () => number }
export const min = Math.min as (...n: Numeric[]) => number
export const max = Math.max as (...n: Numeric[]) => number

export function fix(n: number, max = 15): number {
	if (/^\d+\.\d+$/.test(n.toString())) {
		const position = n.toString().split(".")[1]!.length
		return Number(n.toFixed(position > max ? position - 1 : position))
	} else return n
}

export function clamp<T extends Numeric>(theMin: T, value: T, theMax: T) {
	return min(theMax, max(theMin, value))
}
